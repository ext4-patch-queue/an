ext4: Fix file system hang due to corrupt jinode values.

From: Aneesh Kumar K.V <aneesh.kumar@linux.vnet.ibm.com>

While commiting the transaction we set the i_flags
of jbd2_inode to JI_COMMIT_RUNNING. This is needed to ensure
that when we are commiting the transaction we don't release
the inode via iput. In ext4_clear_inode we wait for the
flag to be cleared before we release the inode.

In ext4_new_inode we call iput for different failure cases
before initializing the jbd2_inode. This cause the failure
case to hang in iput as show below. Fix the same by initializing
jbd2_inode early.

 [<c022d20e>] ? prepare_to_wait+0x49/0x4e
 [<c02cf430>] jbd2_journal_release_jbd_inode+0x86/0xcc
 [<c022d0e4>] ? wake_bit_function+0x0/0x43
 [<c02b329b>] ext4_clear_inode+0x45/0x4a
 [<c02748f7>] clear_inode+0x5f/0xae
 [<c0274e8f>] generic_drop_inode+0x119/0x13a
 [<c0274071>] iput+0x4b/0x4e
 [<c02ab200>] ext4_new_inode+0xb29/0xb7a
 [<c0272761>] ? dput+0x19/0xdc
 [<c02cca5f>] ? jbd2_journal_start+0x8c/0xb9
 [<c02b1cb4>] ext4_create+0x81/0xe7
 [<c02b1c33>] ? ext4_create+0x0/0xe7
 [<c026aa3c>] vfs_create+0x6c/0xdb
 [<c026cfd6>] do_filp_open+0x1a6/0x6b5
 [<c026284e>] do_sys_open+0x40/0xb6
 [<c0262906>] sys_open+0x1e/0x26
 [<c0202be6>] syscall_call+0x7/0xb

Reported-by: Holger Kiehl <Holger.Kiehl@dwd.de>
Signed-off-by: Aneesh Kumar K.V <aneesh.kumar@linux.vnet.ibm.com>
Signed-off-by: Mingming Cao <cmm@us.ibm.com>
Acked-by: Jan Kara <jack@suse.cz>
---

 fs/ext4/ialloc.c  |    1 -
 fs/ext4/inode.c   |    1 -
 fs/ext4/mballoc.c |    2 --
 fs/ext4/super.c   |    1 +
 4 files changed, 1 insertion(+), 4 deletions(-)


Index: linux-2.6.26-rc6/fs/ext4/ialloc.c
===================================================================
--- linux-2.6.26-rc6.orig/fs/ext4/ialloc.c	2008-06-17 10:43:36.000000000 -0700
+++ linux-2.6.26-rc6/fs/ext4/ialloc.c	2008-06-17 10:43:36.000000000 -0700
@@ -820,7 +820,6 @@ got:
 	ei->i_state = EXT4_STATE_NEW;
 
 	ei->i_extra_isize = EXT4_SB(sb)->s_want_extra_isize;
-	jbd2_journal_init_jbd_inode(&ei->jinode, inode);
 
 	ret = inode;
 	if(DQUOT_ALLOC_INODE(inode)) {
Index: linux-2.6.26-rc6/fs/ext4/inode.c
===================================================================
--- linux-2.6.26-rc6.orig/fs/ext4/inode.c	2008-06-17 10:43:36.000000000 -0700
+++ linux-2.6.26-rc6/fs/ext4/inode.c	2008-06-17 10:43:36.000000000 -0700
@@ -2704,7 +2704,6 @@ struct inode *ext4_iget(struct super_blo
 	ei->i_default_acl = EXT4_ACL_NOT_CACHED;
 #endif
 	ei->i_block_alloc_info = NULL;
-	jbd2_journal_init_jbd_inode(&ei->jinode, inode);
 
 	ret = __ext4_get_inode_loc(inode, &iloc, 0);
 	if (ret < 0)
Index: linux-2.6.26-rc6/fs/ext4/mballoc.c
===================================================================
--- linux-2.6.26-rc6.orig/fs/ext4/mballoc.c	2008-06-17 10:43:36.000000000 -0700
+++ linux-2.6.26-rc6/fs/ext4/mballoc.c	2008-06-17 10:43:36.000000000 -0700
@@ -2255,8 +2255,6 @@ static int ext4_mb_init_backend(struct s
 		printk(KERN_ERR "EXT4-fs: can't get new inode\n");
 		goto err_freesgi;
 	}
-	jbd2_journal_init_jbd_inode(&EXT4_I(sbi->s_buddy_cache)->jinode,
-				    sbi->s_buddy_cache);
 	EXT4_I(sbi->s_buddy_cache)->i_disksize = 0;
 
 	metalen = sizeof(*meta_group_info) << EXT4_DESC_PER_BLOCK_BITS(sb);
Index: linux-2.6.26-rc6/fs/ext4/super.c
===================================================================
--- linux-2.6.26-rc6.orig/fs/ext4/super.c	2008-06-17 10:43:36.000000000 -0700
+++ linux-2.6.26-rc6/fs/ext4/super.c	2008-06-17 10:43:36.000000000 -0700
@@ -573,6 +573,7 @@ static struct inode *ext4_alloc_inode(st
 	memset(&ei->i_cached_extent, 0, sizeof(struct ext4_ext_cache));
 	INIT_LIST_HEAD(&ei->i_prealloc_list);
 	spin_lock_init(&ei->i_prealloc_lock);
+	jbd2_journal_init_jbd_inode(&ei->jinode, &ei->vfs_inode);
 	return &ei->vfs_inode;
 }
 
