ext4: Add sparse annotations for the group info semaphore

From: "Aneesh Kumar K.V" <aneesh.kumar@linux.vnet.ibm.com>

Signed-off-by: Aneesh Kumar K.V <aneesh.kumar@linux.vnet.ibm.com>
Signed-off-by: "Theodore Ts'o" <tytso@mit.edu>
---
 fs/ext4/mballoc.c |   24 ++++++++++++++++++++----
 1 files changed, 20 insertions(+), 4 deletions(-)

diff --git a/fs/ext4/mballoc.c b/fs/ext4/mballoc.c
index 9211397..f4753bd 100644
--- a/fs/ext4/mballoc.c
+++ b/fs/ext4/mballoc.c
@@ -895,6 +895,7 @@ static int ext4_mb_init_cache(struct page *page, char *incore)
 static noinline_for_stack int
 ext4_mb_load_buddy(struct super_block *sb, ext4_group_t group,
 					struct ext4_buddy *e4b)
+__acquires(e4b->alloc_semp)
 {
 	int blocks_per_page;
 	int block;
@@ -926,6 +927,7 @@ ext4_mb_load_buddy(struct super_block *sb, ext4_group_t group,
 	 * till we are done with allocation
 	 */
 	down_read(e4b->alloc_semp);
+	__acquire(e4b->alloc_semp);
 
 	/*
 	 * the buddy cache inode stores the block bitmap
@@ -1020,6 +1022,7 @@ ext4_mb_load_buddy(struct super_block *sb, ext4_group_t group,
 }
 
 static void ext4_mb_release_desc(struct ext4_buddy *e4b)
+__releases(e4b->alloc_semp)
 {
 	if (e4b->bd_bitmap_page)
 		page_cache_release(e4b->bd_bitmap_page);
@@ -1027,6 +1030,7 @@ static void ext4_mb_release_desc(struct ext4_buddy *e4b)
 		page_cache_release(e4b->bd_buddy_page);
 	/* Done with the buddy cache */
 	up_read(e4b->alloc_semp);
+	__release(e4b->alloc_semp);
 }
 
 
@@ -1468,8 +1472,10 @@ static int ext4_mb_try_best_found(struct ext4_allocation_context *ac,
 
 	BUG_ON(ex.fe_len <= 0);
 	err = ext4_mb_load_buddy(ac->ac_sb, group, e4b);
-	if (err)
+	if (err) {
+		__release(e4b->alloc_semp);
 		return err;
+	}
 
 	ext4_lock_group(ac->ac_sb, group);
 	max = mb_find_extent(e4b, 0, ex.fe_start, ex.fe_len, &ex);
@@ -1499,8 +1505,10 @@ static int ext4_mb_find_by_goal(struct ext4_allocation_context *ac,
 		return 0;
 
 	err = ext4_mb_load_buddy(ac->ac_sb, group, e4b);
-	if (err)
+	if (err) {
+		__release(e4b->alloc_semp);
 		return err;
+	}
 
 	ext4_lock_group(ac->ac_sb, group);
 	max = mb_find_extent(e4b, 0, ac->ac_g_ex.fe_start,
@@ -1959,8 +1967,10 @@ ext4_mb_regular_allocator(struct ext4_allocation_context *ac)
 				continue;
 
 			err = ext4_mb_load_buddy(sb, group, &e4b);
-			if (err)
+			if (err) {
+				__release(e4b->alloc_semp);
 				goto out;
+			}
 
 			ext4_lock_group(sb, group);
 			if (!ext4_mb_good_group(ac, group, cr)) {
@@ -2271,6 +2281,7 @@ static int ext4_mb_seq_groups_show(struct seq_file *seq, void *v)
 		sizeof(struct ext4_group_info);
 	err = ext4_mb_load_buddy(sb, group, &e4b);
 	if (err) {
+		__release(e4b->alloc_semp);
 		seq_printf(seq, "#%-5u: I/O error\n", group);
 		return 0;
 	}
@@ -3816,6 +3827,7 @@ ext4_mb_discard_group_preallocations(struct super_block *sb,
 
 	err = ext4_mb_load_buddy(sb, group, &e4b);
 	if (err) {
+		__release(e4b->alloc_semp);
 		ext4_error(sb, __func__, "Error in loading buddy "
 				"information for %u\n", group);
 		put_bh(bitmap_bh);
@@ -3983,6 +3995,7 @@ void ext4_discard_preallocations(struct inode *inode)
 
 		err = ext4_mb_load_buddy(sb, group, &e4b);
 		if (err) {
+			__release(e4b->alloc_semp);
 			ext4_error(sb, __func__, "Error in loading buddy "
 					"information for %u\n", group);
 			continue;
@@ -4255,6 +4268,7 @@ ext4_mb_discard_lg_preallocations(struct super_block *sb,
 
 		ext4_get_group_no_and_offset(sb, pa->pa_pstart, &group, NULL);
 		if (ext4_mb_load_buddy(sb, group, &e4b)) {
+			__release(e4b->alloc_semp);
 			ext4_error(sb, __func__, "Error in loading buddy "
 					"information for %u\n", group);
 			continue;
@@ -4715,8 +4729,10 @@ void ext4_mb_free_blocks(handle_t *handle, struct inode *inode,
 	}
 
 	err = ext4_mb_load_buddy(sb, block_group, &e4b);
-	if (err)
+	if (err) {
+		__release(e4b->alloc_semp);
 		goto error_return;
+	}
 	if (metadata) {
 		/* blocks being freed are metadata. these blocks shouldn't
 		 * be used until this transaction is committed */
-- 
1.6.0.3.514.g2f91b

--
To unsubscribe from this list: send the line "unsubscribe linux-ext4" in
the body of a message to majordomo@vger.kernel.org
More majordomo info at  http://vger.kernel.org/majordomo-info.html

